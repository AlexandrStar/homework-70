import React from 'react';
import {StyleSheet, TouchableOpacity, Text} from 'react-native';

export default class InputNumberButton extends React.Component {

  render() {
    const {value, handlerOnPress} = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => handlerOnPress(value)}
      >
        <Text style={styles.text}>{value}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 1,
    backgroundColor: "rgba(255, 255, 255, 0.1)",
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: 'white',
    fontSize: 26,
  }
});