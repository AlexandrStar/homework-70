const initialState = {
  value: '',
};

const reducer = (state = initialState, action) => {
  if (action.type === 'ADD_ELEM') {
    return {
      ...state,
      value: state.value + action.value
    }
  }
  if (action.type === 'CLEAR') {
    return {
      ...state,
      value: ''
    };
  }
  if (action.type === 'EQUAL') {
    return {
      ...state,
      value: eval(state.value)
    };
  }

  return state;
};

export default reducer;